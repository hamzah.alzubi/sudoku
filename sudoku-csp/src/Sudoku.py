from Field import Field

class Sudoku():
    def __init__(self, filename, optimization):
        self.optimization = optimization
        self.board = self.readsudoku(filename)

    def __str__(self):
        output = "╔═══════╦═══════╦═══════╗\n"
        for i in range(0,9):
            if i == 3 or i == 6:
                output += "╠═══════╬═══════╬═══════╣\n"
            output += "║ "
            for j in range(0,9):
                if j == 3 or j == 6:
                    output += "║ "
                output += str(self.board[i][j]) + " "
            output += "║\n"
        output += "╚═══════╩═══════╩═══════╝\n"
        return output

    """
    Reads sudoku from file

    @param filename
    @return 2d int array of the sudoku
    """
    def readsudoku(self, filename):
        assert filename is not None and filename != "", "Invalid file name"

        grid = None
        try:
            # fancy one-liner for fun to read the grid 
            grid = [list(map(Field, map(int, x))) for x in list(map(list, open(filename).read().split()))]
        except FileNotFoundError:
            print(f"Error opening file {filename}")
        self.addneighbors(grid)
        return grid

    """
    Adds a list of neighbors to each field, i.e., arcs to be satisfied

    @param grid
    """
    def addneighbors(self, grid):
        for r in range(9):
            for c in range(9):
                if grid[r][c].value == 0 or not self.optimization: # only need to worry about unset fields
                    neighbors = []

                    # the neighbors in the same block
                    blockStartR = r-r%3
                    blockStartC = c-c%3
                    for neighborR in range(blockStartR, blockStartR+3):
                        for neighborC in range(blockStartC, blockStartC+3):
                            if neighborR != r and neighborC != c:
                                neighbors.append(grid[neighborR][neighborC])

                    # the neighbors in the same row
                    for neighborR in range(9):
                        if neighborR != r:
                            neighbors.append(grid[neighborR][c])

                    # the neighbors in the same column
                    for neighborC in range(9):
                        if neighborC != c:
                            neighbors.append(grid[r][neighborC])
                        
                    grid[r][c].neighbors = neighbors
                        
    """
    Generates fileformat output
    """
    def toFileString(self):
        return "\n".join(["".join(list(map(str, map(Field.getValue,x)))) for x in self.board])