#!/usr/bin/python

from Game import Game
from Sudoku import Sudoku

import argparse

"""
Start AC-3 using the sudoku from the given filepath, and reports whether the sudoku could be solved or not, and how many steps the algorithm performed

@param filePath
@param heuristic
"""
def start(filePath, heuristic, optimization):
    sudoku = Sudoku(filePath, optimization)
    game = Game(sudoku, heuristic, optimization)
    game.showSudoku()
    if game.solve() and game.validSolution():
        print("Solved!")
    else:
        print("Cound not solve this sudoku :(")
    game.showSudoku()
    print(f"The AC-3 algorithm finished after {game.iterations} iterations.")

if __name__ == "__main__":
    # parse the arguments given to the program
    parser = argparse.ArgumentParser(description="Sudoku AC-3 Solver", formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument("filename", metavar="filename", type=str, help="the path to the file that contains the sudoku puzzle to solve")
    parser.add_argument("-heuristic", required=False, metavar="H", type=str.lower, choices={"none", "mrv", "fin", "maxrv", "mnrv", "mnrvfin", "mnrvmaxrv"}, default="mnrvmaxrv", 
                        help="the heuristic to use for the AC-3 algorithm. default=finmrv\
                        \nnone: no heuristic\
                        \nmrv: minimum remaining values heuristic (without tiebreaker)\
                        \nfin: priority to constraints that have arcs to finalized fields heuristic (without tiebreaker)\
                        \nmrvfin: minimum remaining values heuristic (with priority to constraints that have arcs to finalized fields heuristic as tiebreaker)\
                        \nfinmrv: priority to constraints that have arcs to finalized fields heuristic (with minimum remaining values heuristic as tiebreaker)")
    parser.add_argument("-nooptimization", required=False, action='store_true', help="don't use optimizations")
    args = parser.parse_args()

    # create the game and try to solve it (notice the beautiful double negation in "not nooptimization", we have it so the code is optimized by default :))
    start(args.filename, args.heuristic, not args.nooptimization)