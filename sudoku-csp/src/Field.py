class Field():
    def __init__(self, initValue):
        self.neighbors = [] #A list of all fields that this field is constrained by

        if initValue != 0:
            self.value = initValue
            self.domain = [initValue]
        else:
            self.value = 0
            self.domain = list(range(1,10))

    def getValue(self):
        return self.value

    """
    Removes the given value from the domain, and possibly assigns the last value to the field.
    
    @param value
    @return true if the value was removed
    """
    def removeFromDomain(self, value):
        success = True
        
        try:
            self.domain.remove(value)
        except ValueError:
            success = False

        # if there is only one value left in the domain, sets the value of the field to the last domain value.
        if len(self.domain) == 1:
            self.value = self.domain[0]

        return success

    def __str__(self):
        return "." if self.value == 0 else str(self.value)
