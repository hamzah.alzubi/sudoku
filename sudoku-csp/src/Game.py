from Sudoku import Sudoku

class Game:
    def __init__(self, sudoku: Sudoku, heuristic, optimization):
        self.sudoku = sudoku
        self.heuristic = heuristic
        self.arcs = [] # a list of arcs for the AC-3 algorithm to use as a priority queue (we sort this list whenever it is changed)
        self.addArcs()
        self.iterations = 0
        self.optimization = optimization

    def showSudoku(self):
        print(self.sudoku)

    """
    Add all the initial constraints to the list of arcs and sort it according to the heuristic being used
    """
    def addArcs(self):
        grid = self.sudoku.board
        for field in [field for row in grid for field in row]:
            for neighbor in field.neighbors:
                self.arcs.append((field, neighbor))
        self.sortArcs()

    """
    Sorts the list of arcs based on the heuristic being used
    """
    def sortArcs(self):
        # decide the sorting key based on the heuristic
        key = None
        if self.heuristic == "none":
            return
        elif self.heuristic == "mrv":
            # minimum remaining values heuristic (without tiebreaker)
            key = lambda arc : len(arc[0].domain)
        elif self.heuristic == "maxrv":
            # maximum remaining values heuristic (without tiebreaker)
            key = lambda arc : -len(arc[0].domain)
        elif self.heuristic == "mnrv":
            # minimum neighbor remaining values (without tiebreaker)
            key = lambda arc : len(arc[1].domain)
        elif self.heuristic == "fin":
            # priority to constraints that have arcs to finalized fields heuristic (without tiebreaker)
            key = lambda arc : arc[1].value == 0
        elif self.heuristic == "mnrvmaxrv":
            # minimum neighbor remaining values (with maximum remaining values heuristic as tiebreaker)
            key = lambda arc : (len(arc[1].domain), -len(arc[0].domain))

        self.arcs.sort(key=key)

    """
    Implementation of the AC-3 algorithm
    
    @return true if the constraints can be satisfied, else false
    """
    def solve(self):
        while len(self.arcs)>0:
            self.iterations += 1

            # pop the next arc from the queue
            arc = self.arcs[0]
            del self.arcs[0]
            
            oldDomainSize = len(arc[0].domain)
            self.revise(arc)
            newDomainSize = len(arc[0].domain)

            # if this field already has no possible satisfactory value, fail
            if newDomainSize == 0:
                return False

            # if the domain of this field changed, add arcs to it to the queue if not already there
            if newDomainSize != oldDomainSize:
                for neighbor in arc[0].neighbors:
                    if neighbor is not arc[1] and (neighbor, arc[0]) not in self.arcs and (not self.optimization or neighbor.value == 0):
                        self.arcs.append((neighbor, arc[0]))
                        self.sortArcs()
        return True

    """
    Remove the values from the domain of the first field in the arc for which there is no matching value in the domain of the
    second field in the arc such that the constraint is satisfied 
    """
    def revise(self, arc):
        for v1 in arc[0].domain:
            if not any([v2!=v1 for v2 in arc[1].domain]):
                arc[0].removeFromDomain(v1)
             

    """
    Checks the validity of a sudoku solution
    
    @return true if the sudoku solution is correct
    """
    def validSolution(self):
        board = self.sudoku.board
        for r in range(9):
            for c in range(9):
                field = board[r][c]

                # if a field is not set, the solution is not valid
                if field.value == 0:
                    return False
                
                # if a constraint is not satisfied, the solution is not valid
                for neighbor in field.neighbors:
                    if field.value == neighbor.value:
                        return False
        return True
